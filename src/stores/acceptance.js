import Vue from 'vue';
import groupBy from 'lodash/groupBy';

export default {
  namespaced: true,
  state: {
    positions: [
      { id: 1, name: 'P #1', newStatus: 'accepted' },
      { id: 2, name: 'P #2', newStatus: 'accepted' },
      { id: 3, name: 'P #3', newStatus: 'accepted' },
      { id: 4, name: 'P #4', newStatus: 'accepted' },
    ],

    providers: [
      { id: 100, name: 'GLS', variants: [{ id: 'gls-1', name: '1-5' }, { id: 'gls-2', name: '5-10' }] },
      { id: 101, name: 'DHL', variants: [{ id: 'dhl-1', name: '1-5' }, { id: 'dhl-2', name: '5-10' }] },
    ],

    selectedProvider: 100,
  },

  getters: {
    positionsByGroup: (state) => {
      const grouped = groupBy(state.positions, 'newStatus');

      return {
        accepted: grouped.accepted || [],
        declined: grouped.declined || [],
      };
    },

    noSelectedPositionsStatus: (state, getters) => (getters.positionsByGroup.accepted.length + getters.positionsByGroup.declined.length) !== state.positions.length,

    nonePositionIsAccepted: (state, getters) => getters.positionsByGroup.accepted.length === 0,

    noSelectedProvider: state => state.selectedProvider !== null,

    areProvidersFetched: state => state.providers !== null,
  },

  mutations: {
    selectPosition(state, payload) {
      const index = state.positions.findIndex(position => (position.id === payload.id));

      Vue.set(state.positions[index], 'newStatus', payload.value);
    },

    selectProvider(state, payload) {
      state.selectedProvider = payload.id;
    },

    fetchProvidersSuccess(state, providers) {
      state.providers = providers;
    },
  },

  actions: {
    SELECT_POSITION({ commit }, payload) {
      commit('selectPosition', { id: payload.id, value: payload.value });
    },

    SELECT_PROVIDER({ commit }, payload) {
      commit('selectProvider', { id: payload.id });
    },

    FETCH_PROVIDERS({ commit }) {
      return new Promise((resolve, reject) => {
        console.log('REQUEST:', '/fetch-providers');
        setTimeout(() => {
          commit('fetchProvidersSuccess', [
            { id: 100, name: 'GLS', variants: [{ id: 'gls-1', name: '1-5' }, { id: 'gls-2', name: '5-10' }] },
            { id: 101, name: 'DHL', variants: [{ id: 'dhl-1', name: '1-5' }, { id: 'dhl-2', name: '5-10' }] },
          ]);
          resolve({});
        }, 3210);
      });
    },

    UPDATE_ORDER({ getters, commit }) {
      return new Promise((resolve, reject) => {
        console.log('REQUEST:', '/update-order', getters.positionsByGroup);
        setTimeout(() => {
          resolve({});
        }, 500);
      });
    },

    BOOK_SHIPPING({ state, commit }) {
      return new Promise((resolve, reject) => {
        console.log('REQUEST:', '/book-shipping', state.selectedProvider);
        setTimeout(() => {
          resolve({});
        }, 1234);
      });
    },
  },
};
