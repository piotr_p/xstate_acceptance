import { Machine } from 'xstate';

export default Machine({
  id: 'acceptance',
  initial: 'selectPositions',

  states: {
    selectPositions: {
      on: {
        CONTINUE: [
          { cond: 'noSelectedPositionsStatus', target: '.noSelectedPositionsStatus' },
          { cond: 'nonePositionIsAccepted', target: '#acceptance.updatingOrder' },
          { target: 'selectProviders' },
        ],
        SELECT_POSITION: {
          actions: 'SELECT_POSITION',
        },
      },

      initial: 'selecting',
      states: {
        selecting: {},
        noSelectedPositionsStatus: {
          on: {
            SELECT_POSITION: {
              target: '#acceptance.selectPositions.selecting',
              actions: 'SELECT_POSITION',
            },
          },
        },
      },
    },

    selectProviders: {
      on: {
        CONTINUE: 'updatingOrder',
        SELECT_PROVIDER: {
          actions: 'SELECT_PROVIDER',
        },
      },
    },

    updatingOrder: {
      initial: 'inProgress',
      states: {
        inProgress: {
          invoke: {
            id: 'updateOrder',
            src: 'UPDATE_ORDER',
            onDone: '#acceptance.orderUpdated',
            onError: 'failed',
          },
        },
        failed: {
          on: {
            RETRY: '#acceptance.updatingOrder.inProgress',
          },
        },
      },
    },

    orderUpdated: {
      on: {
        '': [
          { cond: 'nonePositionIsAccepted', target: '#acceptance.orderAccepted' },
          { target: 'bookingShipping' },
        ],
      },
    },

    bookingShipping: {
      initial: 'inProgress',
      states: {
        inProgress: {
          invoke: {
            id: 'bookingShipping',
            src: 'BOOK_SHIPPING',
            onDone: '#acceptance.orderAccepted',
            onError: 'failed',
          },
        },
        failed: {
          on: {
            RETRY: '#acceptance.bookingShipping.inProgress',
          },
        },
      },
    },

    orderAccepted: {
      type: 'final',
    },
  },
});
