import Vue from 'vue';
import Vuex from 'vuex';

import AcceptanceModule from '@/stores/acceptance';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },

  modules: {
    acceptance: AcceptanceModule,
  },
});
