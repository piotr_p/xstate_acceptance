import store from '@/store';
import AcceptanceMachine from '@/machines/acceptance';

export default AcceptanceMachine.withConfig({
  actions: {
    SELECT_POSITION: (context, event) => {
      store.dispatch('acceptance/SELECT_POSITION', { id: event.id, value: event.value });
    },

    SELECT_PROVIDER: (context, event) => {
      store.dispatch('acceptance/SELECT_PROVIDER', { id: event.id });
    },


  },

  services: {
    UPDATE_ORDER: () => store.dispatch('acceptance/UPDATE_ORDER'),

    BOOK_SHIPPING: () => store.dispatch('acceptance/BOOK_SHIPPING'),
  },

  guards: {
    noSelectedPositionsStatus: () => store.getters['acceptance/noSelectedPositionsStatus'],
    nonePositionIsAccepted: () => store.getters['acceptance/nonePositionIsAccepted'],
  },
});
